package com.saifan.wyy.network;

import com.android.volley.VolleyError;

import java.util.List;

/**
 * loma
 * Created by Administrator on 2015/6/24.
 */
public interface RequestJsonListener<T> {
    /**
     * 成功
     *
     *
     */
    void requestSuccess(List<T> result);

    /**
     * 错误
     */
    void requestError(VolleyError e);
}
