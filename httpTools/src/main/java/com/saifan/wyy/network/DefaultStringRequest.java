package com.saifan.wyy.network;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by ma on 2015/6/24.
 */

public class DefaultStringRequest extends JsonRequest<JSONObject> {
    private static final int SOCKET_TIMEOUT = 15000;

    /**
     * Creates a new request.
     * @param method the HTTP method to use
     * @param url URL to fetch the JSON from
     * @param parms A {@link JSONObject} to post with the request. Null is allowed and
     *   indicates no parameters will be posted along with request.
     * @param listener Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public DefaultStringRequest(int method, String url, String parms,   Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, (parms == null) ? null : parms, listener,   errorListener);

    }


    /**
     * Creates a new request.
     * @param method the HTTP method to use
     * @param url URL to fetch the JSON from
     * @param map A {@link JSONObject} to post with the request. Null is allowed and
     *   indicates no parameters will be posted along with request.
     * @param listener Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public DefaultStringRequest(int method, String url, Map map,   Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, (map == null) ? null : JSON.toJSONString(map), listener,   errorListener);

    }

    public static Map parserToMap(JSONObject json) {
        try {
            Map map = new HashMap();
            Iterator keys = json.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                Object value = json.get(key);
                map.put(key, value.toString());
            }
            return map;
        } catch (Exception e) {
            Log.e("DefaultStringRequest", e.getLocalizedMessage());
            return null;
        }

    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        RetryPolicy retryPolicy = new DefaultRetryPolicy(SOCKET_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        return retryPolicy;
    }
}


