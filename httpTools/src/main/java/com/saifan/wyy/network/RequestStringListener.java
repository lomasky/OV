package com.saifan.wyy.network;

import com.android.volley.VolleyError;

/**
 * loma
 * Created by Administrator on 2015/6/24.
 */
public interface RequestStringListener  {
    /**
     * 成功
     *
     *
     */
    void requestSuccess(String result);

    /**
     * 错误
     */
    void requestError(VolleyError e);
}
