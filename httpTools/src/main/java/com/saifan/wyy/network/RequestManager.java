package com.saifan.wyy.network;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.saifan.wyy.utils.CommonUtils;
import com.saifan.wyy.utils.JsonUtils;
import com.saifan.wyy.utils.StringUtil;
import com.saifan.wyy.utils.ToastUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * loma
 * Created by Administrator on 2015/6/24.
 */
@SuppressLint("NewApi")
public class RequestManager {
    public static Context context;
    public static LoadingFragment dialog = null;
    private static RequestQueue mRequestQueue;

    private RequestManager() {
    }

    public static RequestQueue getmRequestQueue() {

        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return mRequestQueue;
    }

    /**
     * 返回对象 带进度条
     *
     * @param url      接口
     * @param context  上下文
     * @param clazz    类对象
     * @param params   post需要传的参数
     * @param tip      进度条文字
     * @param listener 回调
     */
    public static <T> void post(final Context context, String url, JSONObject params, final String tip, final Class<T> clazz, final RequestJsonListener<T> listener) {

        if (!CommonUtils.isNetWorkConnected(context)) {
            ToastUtil.showToast(context, "网络异常");

            return;
        }

        if (!StringUtil.isNullOrEmpty(tip)) {
            dialog = new LoadingFragment();
            dialog.show(((FragmentActivity) context).getSupportFragmentManager(), "Loading");
            dialog.setMsg(tip);
        }
        DefaultJsonRequest jsonObjectRequest = new DefaultJsonRequest(url, params, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
                try {
                    String code = response.getString("Code");
                    if (code.equals("200")) {
                        List<T> list = JsonUtils.toList(response.getString("Content"), clazz);
//                        JSONArray jsonArray = response.getJSONArray("Content");
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            JSONObject o = jsonArray.getJSONObject(i);
//                            list.add(JsonUtils.object(o.toString(), clazz));
//                        }


                        listener.requestSuccess(list);
                    } else if (code.equals("203")) {

                        ToastUtil.showToast(context, "密码错误");
                    } else if (code.equals("204")) {

                        ToastUtil.showToast(context, "用户名已存在 !");
                    } else if (code.equals("205")) {

                        ToastUtil.showToast(context, "信息已经提交过了 !");
                    } else if (code.equals("202")) {

                        ToastUtil.showToast(context, "保存失败，信息未做修改 !");
                    } else {

                        ToastUtil.showToast(context, "服务器出错了");

                    }
                } catch (JSONException e) {
                    ToastUtil.showToast(context, "服务器出错了");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.requestError(error);
                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
            }
        });

        addRequest(jsonObjectRequest, context);
    }


    /**
     * 返回对象 带进度条
     *
     * @param url      接口
     * @param fragment  上下文
     * @param clazz    类对象
     * @param params   post需要传的参数
     * @param tip      进度条文字
     * @param listener 回调
     */
    public static <T> void post(final Fragment fragment, String url, JSONObject params, final String tip, final Class<T> clazz, final RequestJsonListener<T> listener) {

        if (!CommonUtils.isNetWorkConnected(fragment.getActivity())) {
            ToastUtil.showToast(fragment.getActivity(), "网络异常");

            return;
        }

        if (!StringUtil.isNullOrEmpty(tip)) {
            dialog = new LoadingFragment();
            dialog.show(fragment.getActivity().getSupportFragmentManager(), "Loading");
            dialog.setMsg(tip);
        }
        DefaultJsonRequest jsonObjectRequest = new DefaultJsonRequest(url, params, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
                try {
                    String code = response.getString("Code");
                    if (code.equals("200")) {
                        List<T> list = JsonUtils.toList(response.getString("Content"), clazz);
//                        JSONArray jsonArray = response.getJSONArray("Content");
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            JSONObject o = jsonArray.getJSONObject(i);
//                            list.add(JsonUtils.object(o.toString(), clazz));
//                        }


                        listener.requestSuccess(list);
                    } else if (code.equals("203")) {

                        ToastUtil.showToast(fragment.getActivity(), "密码错误");
                    } else if (code.equals("204")) {

                        ToastUtil.showToast(fragment.getActivity(), "用户名已存在 !");
                    } else if (code.equals("205")) {

                        ToastUtil.showToast(fragment.getActivity(), "信息已经提交过了 !");
                    } else if (code.equals("202")) {

                        ToastUtil.showToast(fragment.getActivity(), "保存失败，信息未做修改 !");
                    } else {

                        ToastUtil.showToast(fragment.getActivity(), "服务器出错了");

                    }
                } catch (JSONException e) {
                    ToastUtil.showToast(fragment.getActivity(), "服务器出错了");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.requestError(error);
                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
            }
        });

        addRequest(jsonObjectRequest, fragment);
    }

    /**
     * 返回对象 带进度条
     *
     * @param url      接口
     * @param context  上下文
     * @param clazz    类对象
     * @param params   post需要传的参数
     * @param tip      进度条文字
     * @param listener 回调
     */
    public static <T> void post(final Context context, String url, Object params, final String tip, final Class<T> clazz, final RequestJsonListener<T> listener) {

        if (!CommonUtils.isNetWorkConnected(context)) {
            ToastUtil.showToast(context, "网络异常");

            return;
        }

        if (!StringUtil.isNullOrEmpty(tip)) {
            dialog = new LoadingFragment();
            dialog.show(((FragmentActivity) context).getSupportFragmentManager(), "Loading");
            dialog.setMsg(tip);
        }
        DefaultJsonRequest jsonObjectRequest = new DefaultJsonRequest(url, params, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
                try {
                    String code = response.getString("Code");
                    if (code.equals("200")) {
                        List<T> list = JsonUtils.toList(response.getString("Content"), clazz);
//                        JSONArray jsonArray = response.getJSONArray("Content");
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            JSONObject o = jsonArray.getJSONObject(i);
//                            list.add(JsonUtils.object(o.toString(), clazz));
//                        }

                        listener.requestSuccess(list);


                    } else if (code.equals("203")) {

                        ToastUtil.showToast(context, "密码错误");
                    } else if (code.equals("204")) {

                        ToastUtil.showToast(context, "用户名已存在 !");
                    } else if (code.equals("205")) {

                        ToastUtil.showToast(context, "信息已经提交过了 !");
                    } else if (code.equals("202")) {

                        ToastUtil.showToast(context, "保存失败，信息未做修改 !");
                    } else {

                        ToastUtil.showToast(context, "服务器出错了");

                    }
                } catch (JSONException e) {
                    ToastUtil.showToast(context, "服务器出错了");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.requestError(error);
                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
            }
        });

        addRequest(jsonObjectRequest, context);
    }

    /**
     * 返回对象 带进度条
     *
     * @param url      接口
     * @param context  上下文
     * @param params   post需要传的参数
     * @param tip      进度条文字
     * @param listener 回调
     */
    public static <T> void post(final Context context, String url, String params, final String tip, final RequestStringListener listener) {

        if (!CommonUtils.isNetWorkConnected(context)) {
            ToastUtil.showToast(context, "网络异常");
            return;
        }

        if (!StringUtil.isNullOrEmpty(tip)) {
            dialog = new LoadingFragment();
            dialog.show(((FragmentActivity) context).getSupportFragmentManager(), "Loading");
            dialog.setMsg(tip);
        }
        DefaultStringRequest stringRequest = new DefaultStringRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
                try {

                    String code = response.getString("Code");
                    if (code.equals("200")) {
                        String content = response.getString("Content");
                        if (!StringUtil.isNullOrEmpty(content)) {

                            listener.requestSuccess(content);
                        }

                    } else if (code.equals("203")) {

                        ToastUtil.showToast(context, "密码错误");
                    } else if (code.equals("204")) {

                        ToastUtil.showToast(context, "用户名已存在 !");
                    } else if (code.equals("205")) {

                        ToastUtil.showToast(context, "信息已经提交过了 !");
                    } else if (code.equals("202")) {

                        ToastUtil.showToast(context, "保存失败，信息未做修改 !");
                    } else {

                        ToastUtil.showToast(context, "服务器出错了");

                    }
                } catch (Exception e) {
                    ToastUtil.showToast(context, "服务器出错了");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.requestError(error);
                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
            }
        });

        addRequest(stringRequest, context);
    }


    public static void addRequest(Request<?> request, Object tag) {
        if (tag != null) {
            request.setTag(tag);
        }
        if (context==null){
            if (tag instanceof Context){
                context=(Context)tag;
            }
            else if (tag instanceof Fragment){
               context= ((Fragment)tag).getActivity();
            }
        }
        getmRequestQueue().add(request);


    }


    /**
     * 当主页面调用协议 在结束该页面调用此方法
     *
     * @param tag
     */
    public static void cancelAll(Object tag) {

        if (mRequestQueue!=null){
            mRequestQueue.cancelAll(tag);
        }


    }
}
