package com.saifan.wyy.network;

import android.content.Context;
import android.support.v4.app.Fragment;

import org.json.JSONObject;

/**
 * loma
 * Created by Administrator on 2015/6/24.
 */
public class Http {

    /**
     * 返回对象 带进度条 post 可选择显示进度
     *
     * @param context
     * @param method
     * @param clazz
     * @param params
     * @param tip
     * @param listener
     */
    public static <T> void post(Context context, String method,  JSONObject params, String tip,final Class<T> clazz, final RequestJsonListener<T> listener) {
        RequestManager.post(context, URLConfig.URL_ROOT + method,  params, tip, clazz,listener);
    }
    /**
     * 返回对象 带进度条 post 可选择显示进度
     *
     * @param context
     * @param method
     * @param clazz
     * @param params
     * @param tip
     * @param listener
     */
    public static <T> void post(Fragment context, String method,  JSONObject params, String tip,final Class<T> clazz, final RequestJsonListener<T> listener) {
        RequestManager.post(context, URLConfig.URL_ROOT + method,  params, tip, clazz,listener);
    }
    /**
     * 返回对象 带进度条 post 可选择显示进度
     *
     * @param context
     * @param method
     * @param clazz
     * @param params
     * @param tip
     * @param listener
     */
    public static <T> void post(Context context, String method, Object params, String tip,  final Class<T> clazz,final RequestJsonListener<T> listener) {
        RequestManager.post(context, URLConfig.URL_ROOT + method, params, tip,  clazz,listener);
    }


    /**
     * 返回对象 带进度条 post 可选择显示进度
     *
     * @param context
     * @param method
     * @param params
     * @param tip
     * @param listener
     */
    public static <T> void post(Context context, String method, String params, String tip, final RequestStringListener listener) {
        RequestManager.post(context, URLConfig.URL_ROOT + method, params, tip, listener);
    }

    public  static  void cancel(Object object){

        RequestManager.cancelAll(object);
    }


}
