package com.saifan.wyy.ov;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.saifan.wyy.network.Http;
import com.saifan.wyy.network.RequestJsonListener;
import com.saifan.wyy.network.URLConfig;
import com.saifan.wyy.ov.data.bean.Room;
import com.saifan.wyy.utils.JsonUtils;
import com.saifan.wyy.utils.ToastUtil;

import org.json.JSONObject;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends BaseFragment {


    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_blank, container, false);
        view.findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                httptest();

            }
        });

        return view;
    }

    public void  httptest(   ){
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("UserName", "5");
            jsonObject.put("Password", "123456");

            Http.post(BlankFragment.this, URLConfig.Customer.LOGIN, jsonObject, "", Room.class, new RequestJsonListener<Room>() {
                @Override
                public void requestSuccess(List<Room> result) {
                    ToastUtil.showToast(getActivity(), JsonUtils.toJson(result));

                }

                @Override
                public void requestError(VolleyError e) {

                }
            });
        }catch (Exception e){

        }

    }


}
