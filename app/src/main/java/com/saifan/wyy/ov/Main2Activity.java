package com.saifan.wyy.ov;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.astuetz.PagerSlidingTabStrip;
import com.saifan.wyy.utils.DensityUtil;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends BaseActivity {


    private final String[] TITLES = {"12", "34"};
    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    private List<BlankFragment> list = new ArrayList<>();

    private int index = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        pager = (ViewPager) findViewById(R.id.pager);
        {
            BlankFragment messageFragment = new BlankFragment();
            list.add(messageFragment);
        }
        {
            BlankFragment messageFragment = new BlankFragment();
            list.add(messageFragment);
        }

        pager.setAdapter(new MyAdapter(getSupportFragmentManager(), TITLES));
        pager.setOffscreenPageLimit(list.size());
        tabs.setTextColor(Color.RED);
        tabs.setIndicatorColor(Color.RED);
        tabs.setIndicatorHeight(DensityUtil.dip2px(getApplicationContext(), 2));
        tabs.setViewPager(pager);

        tabs.setTextSize(DensityUtil.dip2px(getApplicationContext(), 16));


        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                index = arg0;

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public class MyAdapter extends FragmentStatePagerAdapter {
        String[] _titles;

        public MyAdapter(FragmentManager fm, String[] titles) {
            super(fm);
            _titles = titles;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return _titles[position];
        }

        @Override
        public int getCount() {
            return _titles.length;
        }

        @Override
        public Fragment getItem(int position) {
            return list.get(position);
        }

    }
}
