package com.saifan.wyy.ov.data.bean;

/**
 * Created by ma on 2015/7/6.
 */
public class Room {
    /**
     * WYKHDA_ZJHM : null
     * WYKHDA_KHMC : 尹秀容
     * WYKHDA_ZJ : 00100100000000000001
     * WYKHDA_WXH : 经济学家大家
     * WYFJDA_FJMC : 01-01A
     * WYKHDA_LC : James
     * WYKHDA_SJ : 13579246810
     * WYFJDA_ZJ : 00100100000000000001
     * WYFJDA_HX :
     * XMBS : 100100
     * XMDA_XMMC : 新天地大厦
     * WYKHDA_ZJMC : null
     * WYKHDA_TXLJ : /Upload/HeadPhoto/201576/239f21ce-1774-4487-bd86-386ce2457453.jpg
     * WYFJDA_CX : null
     * WYKHDA_KHYHM : 13579246810
     * FKGX_ZJ : 00100100000000000001
     * WYKHDA_XB : null
     * WYFJDA_JZMC : 100
     */
//null

     private byte b;

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    private byte id;
    public byte getB() {
        return b;
    }

    public void setB(byte b) {
        this.b = b;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public Byte getB2() {
        return b2;
    }

    public void setB2(Byte b2) {
        this.b2 = b2;
    }

    public Byte[] getBsytes() {
        return bsytes;
    }

    public void setBsytes(Byte[] bsytes) {
        this.bsytes = bsytes;
    }

    private byte[]bytes;
    private Byte b2;
    private  Byte[] bsytes;
    private String WYKHDA_ZJHM;
    //尹秀容
    private String WYKHDA_KHMC;
    //00100100000000000001
    private String WYKHDA_ZJ;
    //经济学家大家
    private String WYKHDA_WXH;
    //01-01A
    private String WYFJDA_FJMC;
    //James
    private String WYKHDA_LC;
    //13579246810
    private String WYKHDA_SJ;
    //00100100000000000001
    private String WYFJDA_ZJ;
    //
    private String WYFJDA_HX;
    //100100
    private int XMBS;
    //新天地大厦
    private String XMDA_XMMC;
    //null
    private String WYKHDA_ZJMC;
    ///Upload/HeadPhoto/201576/239f21ce-1774-4487-bd86-386ce2457453.jpg
    private String WYKHDA_TXLJ;
    //null
    private String WYFJDA_CX;
    //13579246810
    private String WYKHDA_KHYHM;
    //00100100000000000001
    private String FKGX_ZJ;
    //null
    private String WYKHDA_XB;
    //100
    private int WYFJDA_JZMC;

    public void setWYKHDA_ZJHM(String WYKHDA_ZJHM) {
        this.WYKHDA_ZJHM = WYKHDA_ZJHM;
    }

    public void setWYKHDA_KHMC(String WYKHDA_KHMC) {
        this.WYKHDA_KHMC = WYKHDA_KHMC;
    }

    public void setWYKHDA_ZJ(String WYKHDA_ZJ) {
        this.WYKHDA_ZJ = WYKHDA_ZJ;
    }

    public void setWYKHDA_WXH(String WYKHDA_WXH) {
        this.WYKHDA_WXH = WYKHDA_WXH;
    }

    public void setWYFJDA_FJMC(String WYFJDA_FJMC) {
        this.WYFJDA_FJMC = WYFJDA_FJMC;
    }

    public void setWYKHDA_LC(String WYKHDA_LC) {
        this.WYKHDA_LC = WYKHDA_LC;
    }

    public void setWYKHDA_SJ(String WYKHDA_SJ) {
        this.WYKHDA_SJ = WYKHDA_SJ;
    }

    public void setWYFJDA_ZJ(String WYFJDA_ZJ) {
        this.WYFJDA_ZJ = WYFJDA_ZJ;
    }

    public void setWYFJDA_HX(String WYFJDA_HX) {
        this.WYFJDA_HX = WYFJDA_HX;
    }

    public void setXMBS(int XMBS) {
        this.XMBS = XMBS;
    }

    public void setXMDA_XMMC(String XMDA_XMMC) {
        this.XMDA_XMMC = XMDA_XMMC;
    }

    public void setWYKHDA_ZJMC(String WYKHDA_ZJMC) {
        this.WYKHDA_ZJMC = WYKHDA_ZJMC;
    }

    public void setWYKHDA_TXLJ(String WYKHDA_TXLJ) {
        this.WYKHDA_TXLJ = WYKHDA_TXLJ;
    }

    public void setWYFJDA_CX(String WYFJDA_CX) {
        this.WYFJDA_CX = WYFJDA_CX;
    }

    public void setWYKHDA_KHYHM(String WYKHDA_KHYHM) {
        this.WYKHDA_KHYHM = WYKHDA_KHYHM;
    }

    public void setFKGX_ZJ(String FKGX_ZJ) {
        this.FKGX_ZJ = FKGX_ZJ;
    }

    public void setWYKHDA_XB(String WYKHDA_XB) {
        this.WYKHDA_XB = WYKHDA_XB;
    }

    public void setWYFJDA_JZMC(int WYFJDA_JZMC) {
        this.WYFJDA_JZMC = WYFJDA_JZMC;
    }

    public String getWYKHDA_ZJHM() {
        return WYKHDA_ZJHM;
    }

    public String getWYKHDA_KHMC() {
        return WYKHDA_KHMC;
    }

    public String getWYKHDA_ZJ() {
        return WYKHDA_ZJ;
    }

    public String getWYKHDA_WXH() {
        return WYKHDA_WXH;
    }

    public String getWYFJDA_FJMC() {
        return WYFJDA_FJMC;
    }

    public String getWYKHDA_LC() {
        return WYKHDA_LC;
    }

    public String getWYKHDA_SJ() {
        return WYKHDA_SJ;
    }

    public String getWYFJDA_ZJ() {
        return WYFJDA_ZJ;
    }

    public String getWYFJDA_HX() {
        return WYFJDA_HX;
    }

    public int getXMBS() {
        return XMBS;
    }

    public String getXMDA_XMMC() {
        return XMDA_XMMC;
    }

    public String getWYKHDA_ZJMC() {
        return WYKHDA_ZJMC;
    }

    public String getWYKHDA_TXLJ() {
        return WYKHDA_TXLJ;
    }

    public String getWYFJDA_CX() {
        return WYFJDA_CX;
    }

    public String getWYKHDA_KHYHM() {
        return WYKHDA_KHYHM;
    }

    public String getFKGX_ZJ() {
        return FKGX_ZJ;
    }

    public String getWYKHDA_XB() {
        return WYKHDA_XB;
    }

    public int getWYFJDA_JZMC() {
        return WYFJDA_JZMC;
    }
}
