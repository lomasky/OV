package com.saifan.wyy.ov;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.VolleyError;
import com.loma.xui.ArrayView;
import com.loma.xui.MyAdapter;
import com.saifan.wyy.network.Http;
import com.saifan.wyy.network.RequestJsonListener;
import com.saifan.wyy.network.URLConfig;
import com.saifan.wyy.ov.data.bean.Room;
import com.saifan.wyy.ov.data.bean.UserInfo;
import com.saifan.wyy.ov.databinding.ActivityMainBinding;
import com.saifan.wyy.utils.JsonUtils;
import com.saifan.wyy.utils.ToastUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends BaseActivity {

    List<Room> list = new ArrayList<>();
    private ActivityMainBinding binding;
    private UserInfo user;
    private ArrayView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private MyAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);


        user = new UserInfo();
        user.setUserName("5");
        user.setPassword("123456");
        binding.setUser(user);

        mAdapter = new MyAdapter(list, com.saifan.wyy.ov.BR.room, R.layout.list_item);
        binding.recyclerView.setAdapter(mAdapter);


    }

    /**
     *
     * */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, Main2Activity.class);
            startActivity(intent);
//            login();
//            test();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void test(View view) {
//        TimerTask timerTask = new TimerTask() {
//            @Override
//            public void run() {
//
//                try {
//                    Thread.sleep(10000);
//                    user.setUserName("fadfasfdsfasdfadsfasdf");
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        };
//        timerTask.run();
        login();

    }


    public void login() {

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("UserName","5");
            jsonObject.put("Password","123456");
            binding.button1.setText("正在登录");
            String parms = JsonUtils.toJson(user);
            Log.i("zjt", parms);
            Http.post(this, URLConfig.Customer.LOGIN, user, "Login", Room.class, new RequestJsonListener<Room>() {
                @Override
                public void requestSuccess(List<Room> result) {
                    ToastUtil.showToast(MainActivity.this, JsonUtils.toJson(result));
                    mAdapter.mDatas = result;
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void requestError(VolleyError e) {

                }
            });
        }catch (Exception e){

        }


    }

}
