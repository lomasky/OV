package com.saifan.wyy.ov.data.bean;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.alibaba.fastjson.annotation.JSONField;
import com.saifan.wyy.ov.BR;

import java.io.Serializable;


/**
 * Created by ma on 2015/7/1.
 */
public class UserInfo extends  BaseObservable     {

    /**
     * UserName : 5
     * Password : 123456
     */
//5
    @Bindable
    private String UserName;
    //123456

    @Bindable
    private String Password;

    public UserInfo(String userName, String password) {
        UserName = userName;
        Password = password;
    }

    public UserInfo() {
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
        notifyPropertyChanged(BR.UserName);
    }

    public void setPassword(String Password) {
        this.Password = Password;
        notifyPropertyChanged(BR.Password);
    }


    public String getUserName() {
        return UserName;
    }


    public String getPassword() {
        return Password;
    }
}