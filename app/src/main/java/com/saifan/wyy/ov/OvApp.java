package com.saifan.wyy.ov;

import android.app.Application;

import com.saifan.wyy.utils.CommonUtils;

import java.io.File;

/**
 * Created by ma on 2015/7/20.
 */
public class OvApp extends Application {

    private String AppFile;

    @Override
    public void onCreate() {
        super.onCreate();

        AppFile = CommonUtils.getSDCardPath() + "/ov";
        File file = new File(AppFile);
        // 检测路径是否存在
        if (!file.exists()) {
            file.mkdirs();// 创建该路径
        }
    }
}
