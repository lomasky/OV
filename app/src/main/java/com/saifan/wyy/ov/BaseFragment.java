package com.saifan.wyy.ov;

import android.support.v4.app.Fragment;

import com.saifan.wyy.network.Http;

/**
 * Created by ma on 2015/8/4.
 */
public class BaseFragment extends Fragment {

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(!isVisibleToUser){
            Http.cancel(this);
        }
    }
}
