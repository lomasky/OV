package com.loma.xui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.saifan.wyy.network.URLConfig;
import com.squareup.picasso.Picasso;



/**
 * Created by ma on 2015/6/24.
 * 网络图片显示控件
 *
 */
public class NetworkImg extends ImageView {
    Context context;
    public NetworkImg(Context context) {
        super(context);
        this.context=context;
    }

    public NetworkImg(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        TypedArray a;
        a = context.obtainStyledAttributes(attrs, R.styleable.networkimg);
        String url = a.getString(R.styleable.networkimg_url);
        if (url != null) {
            setUrl(url);

        }


    }

    public NetworkImg(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context=context;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.networkimg);
        String url = a.getString(R.styleable.networkimg_url);
        if (url != null) {
            if (url != null) {
                setUrl(url);

            }

        }
    }

    public void setUrl(String url) {

        if (url != null) {
            if(!url.startsWith("http://")){
                Picasso.with(context).load(URLConfig.URL_IP+url).placeholder(R.mipmap.pic_thumb).error(R.mipmap.pic_thumb)
                        .into(this);
            }else {
                Picasso.with(context).load(url).placeholder(R.mipmap.pic_thumb).error(R.mipmap.pic_thumb)
                        .into(this);
            }

        }

    }
}
