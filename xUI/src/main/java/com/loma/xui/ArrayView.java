package com.loma.xui;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by ma on 2015/7/22.
 */
public class ArrayView extends RecyclerView {

    private   LinearLayoutManager mLayoutManager;

    public ArrayView(Context context) {
        super(context);



    }

    public ArrayView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
        setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(context);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        setLayoutManager(mLayoutManager);
        setItemAnimator(new DefaultItemAnimator());
        addItemDecoration(new ItemDivider(context));
    }

    public ArrayView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(context);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        setLayoutManager(mLayoutManager);
        setItemAnimator(new DefaultItemAnimator());
        addItemDecoration(new ItemDivider(context));
    }
}
